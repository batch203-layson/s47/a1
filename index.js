//console.log("Hello World");

/* 
[SECTION]Document Object Model

//allow us to be able to access or modify the properties of an html element in a webpage
//it is a standard on how to get, change, add, or delete HTML elements
//we will focus on using DOM for managing forms
//For selecting HTML elements will be using the document.querySelector
//SYNTAX:
document.querySelector("htmlElement")
document refers to the whole page
".querySelector" is used to select a specific object (HTML elements) from the document (web page).
The query selector function takes a string input that is formatted like a CSS selector when applying the styles.

//Alternatively, we can use the getElement functions to retrieve the elements
document.getElementById("txt-first-name");

however, using these functions require us to identify beforehand how we get the elements. With querySelector, we can be flexible in how to retireve the elements.
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

/* .red{
    document.getElementById("span-full-name").style.color = "red";
}
.blue{
    document.getElementById("span-full-name").style.color = "blue";
}
.green{
    document.getElementById("span-full-name").style.color = "green";
} */

/* 

.greenText{ background - color: green; }

.blueText{ background - color: blue; }

.redText{ background - color: red; } */
//const textColor = document.querySelector("#text-color");

/*  
[SECTION] Event Listeners
whenever a user interacts with a web page this action is considered as an event
workingwith events is large part of creating interactivity in a webpage
to perform an action when an event triggers, you first need to listen to it.
*/

/* 
// The function use is "addEventListener" that takes two arguments:
a string identifying an event;
and a function that the listener will execute once the "specified event" is triggered.
when an event occurs, an "event object" is passed to the function argument as the first parameter
*/
txtFirstName.addEventListener("keyup", (event)=>{
    console.log(event.target); //contains the element
    console.log(event.target.value); //contains the actual value
});

txtFirstName.addEventListener("keyup", ()=>{
    spanFullName.innerHTML = `${txtFirstName.value}`;
});


const fullName = () => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

function selectColor(i) {
    var color = i.value;
    document.querySelector("#span-full-name").style.color = color;
}
/* 
const textColor = () => {
    
} */

//Activity s47
/*
1. Create an addEventListener that will "change the color" of the "spanFullName". 
2. Add a "select tag" element with the options/values of red, green, and blue in your index.html.
3. The values of the select tag will be use to change the color of the span with the id "spanFullName"

Check the following links to solve this activity:
    HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
    HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/